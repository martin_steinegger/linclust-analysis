time diamond makedb --in uniprot_db_32.fasta -d uniprot_db_32_diamond
time diamond blastp -q uniprot_db_32.fasta -d uniprot_db_32_diamond --id 0.5 -a diamond_2016_03_32_50.uc --evalue 0.001
time diamond makedb --in uniprot_db_16.fasta -d uniprot_db_16_diamond
time diamond blastp -q uniprot_db_16.fasta -d uniprot_db_16_diamond --id 0.5 -a diamond_2016_03_16_50.uc --evalue 0.001
time diamond makedb --in uniprot_db_8.fasta -d uniprot_db_8_diamond
time diamond blastp -q uniprot_db_8.fasta -d uniprot_db_8_diamond --id 0.5 -a diamond_2016_03_8_50.uc --evalue 0.001
time diamond makedb --in uniprot_db_4.fasta -d uniprot_db_4_diamond
time diamond blastp -q uniprot_db_4.fasta -d uniprot_db_4_diamond --id 0.5 -a diamond_2016_03_4_50.uc --evalue 0.001
time diamond makedb --in uniprot_db_2.fasta -d uniprot_db_2_diamond
time diamond blastp -q uniprot_db_4.fasta -d uniprot_db_2_diamond --id 0.5 -a diamond_2016_03_2_50.uc --evalue 0.001
time diamond makedb --in uniprot_db.fasta -d uniprot_db_diamond
time diamond blastp -q uniprot_db_4.fasta -d uniprot_db_diamond --id 0.5 -a diamond_2016_03_50.uc --evalue 0.001  
